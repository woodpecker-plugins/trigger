// Copyright 2020, the Drone Plugins project authors.
// Copyright 2023 Woodpecker Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package trigger

import (
	"time"

	"codeberg.org/woodpecker-plugins/go-plugin"
	"github.com/urfave/cli/v3"
)

// Settings for the plugin.
type Settings struct {
	Repos          []string
	Server         string
	Token          string
	Wait           bool
	Timeout        time.Duration
	LastSuccessful bool
	Params         []string
	ParamsEnv      []string
	Deploy         string

	server string
	params map[string]string
}

// Plugin implements provide the plugin implementation.
type Plugin struct {
	*plugin.Plugin
	Settings Settings
}

// New initializes a plugin from the given Settings, Pipeline, and Network.
func New(version string) *Plugin {
	p := &Plugin{}

	p.Plugin = plugin.New(plugin.Options{
		Name:        "plugin-trigger",
		Description: "Trigger a Woodpecker CI build",
		Version:     version,
		Flags:       p.Flags(),
		Execute:     p.Execute,
	})

	return p
}

func (p *Plugin) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringSliceFlag{
			Name:        "repositories",
			Usage:       "List of repositories to trigger",
			Sources:     cli.EnvVars("PLUGIN_REPOSITORIES"),
			Destination: &p.Settings.Repos,
		},
		&cli.StringFlag{
			Name:        "server",
			Usage:       "Trigger a Woodpecker CI pipeline on a custom server",
			Sources:     cli.EnvVars("PLUGIN_SERVER"),
			Destination: &p.Settings.Server,
		},
		&cli.StringFlag{
			Name:        "token",
			Usage:       "Woocpecker CI API token for server",
			Sources:     cli.EnvVars("PLUGIN_TOKEN"),
			Destination: &p.Settings.Token,
		},
		&cli.BoolFlag{
			Name:        "wait",
			Usage:       "Wait for any currently running builds to finish",
			Sources:     cli.EnvVars("PLUGIN_WAIT"),
			Destination: &p.Settings.Wait,
		},
		&cli.DurationFlag{
			Name:        "timeout",
			Value:       time.Duration(60) * time.Second,
			Usage:       "How long to wait on any currently running builds",
			Sources:     cli.EnvVars("PLUGIN_WAIT_TIMEOUT"),
			Destination: &p.Settings.Timeout,
		},
		&cli.BoolFlag{
			Name:        "last-successful",
			Usage:       "Trigger last successful build",
			Sources:     cli.EnvVars("PLUGIN_LAST_SUCCESSFUL"),
			Destination: &p.Settings.LastSuccessful,
		},
		&cli.StringSliceFlag{
			Name:        "params",
			Usage:       "List of params (key=value or file paths of params) to pass to triggered builds",
			Sources:     cli.EnvVars("PLUGIN_PARAMS"),
			Destination: &p.Settings.Params,
		},
		&cli.StringSliceFlag{
			Name:        "params-from-env",
			Usage:       "List of environment variables to pass to triggered builds",
			Sources:     cli.EnvVars("PLUGIN_PARAMS_FROM_ENV"),
			Destination: &p.Settings.ParamsEnv,
		},
		&cli.StringFlag{
			Name:        "deploy",
			Usage:       "Environment to trigger deploy for the respective build",
			Sources:     cli.EnvVars("PLUGIN_DEPLOY"),
			Destination: &p.Settings.Deploy,
		},
	}
}
