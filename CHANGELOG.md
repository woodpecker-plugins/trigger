# Changelog

## [1.0.0](https://codeberg.org/woodpecker-plugins/trigger/releases/tag/v1.0.0) - 2025-02-15

### ❤️ Thanks to all contributors! ❤️

@Makao, @anbraten, @woodpecker-bot

### 💥 Breaking changes

- Updated woodpecker dependency to v3 [[#35](https://codeberg.org/woodpecker-plugins/trigger/pulls/35)]

### 📦️ Dependency

- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v5.2.0 [[#33](https://codeberg.org/woodpecker-plugins/trigger/pulls/33)]
- chore(deps): update docker.io/woodpeckerci/plugin-ready-release-go docker tag to v3 [[#27](https://codeberg.org/woodpecker-plugins/trigger/pulls/27)]
- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v5 [[#26](https://codeberg.org/woodpecker-plugins/trigger/pulls/26)]

### Misc

- docs: update docs and logo [[#28](https://codeberg.org/woodpecker-plugins/trigger/pulls/28)]

## [0.2.3](https://codeberg.org/woodpecker-plugins/trigger/releases/tag/v0.2.3) - 2024-09-29

### ❤️ Thanks to all contributors! ❤️

@6543, @lafriks, @qwerty287, @woodpecker-bot

### 📚 Documentation

- Add CODEOWNERS [[#20](https://codeberg.org/woodpecker-plugins/trigger/pulls/20)]
- Fix author key [[#18](https://codeberg.org/woodpecker-plugins/trigger/pulls/18)]
- Add logo [[#17](https://codeberg.org/woodpecker-plugins/trigger/pulls/17)]

### 📦️ Dependency

- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v4 [[#21](https://codeberg.org/woodpecker-plugins/trigger/pulls/21)]
- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v3 [[#19](https://codeberg.org/woodpecker-plugins/trigger/pulls/19)]

### Misc

- Use ready-release-go plugin [[#24](https://codeberg.org/woodpecker-plugins/trigger/pulls/24)]
- Migrate to github.com/urfave/cli/v3 [[#23](https://codeberg.org/woodpecker-plugins/trigger/pulls/23)]
