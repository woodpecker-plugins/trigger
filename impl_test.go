// Copyright 2020, the Drone Plugins project authors.
// Copyright 2023 Woodpecker Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package trigger

import (
	"reflect"
	"testing"
)

func TestParseRepoBranch(t *testing.T) {
	tests := []struct {
		Repo   string
		Slug   string
		Branch string
	}{
		{"octocat/hello-world", "octocat/hello-world", ""},
		{"octocat/hello-world@master", "octocat/hello-world", "master"},
	}

	for _, test := range tests {

		slug, branch := parseRepoBranch(test.Repo)
		if slug != test.Slug {
			t.Errorf("wanted repository %s, got %s", test.Slug, slug)
		}
		if branch != test.Branch {
			t.Errorf("wanted repository branch %s, got %s", test.Branch, branch)
		}
	}
}

func TestParseParamsInvalid(t *testing.T) {
	out, err := parseParams([]string{"invalid"})
	if err == nil {
		t.Errorf("expected error, got %v", out)
	}
}

func TestParseParams(t *testing.T) {
	tests := []struct {
		Input  []string
		Output map[string]string
	}{
		{[]string{}, map[string]string{}},
		{
			[]string{"where=far", "who=you"},
			map[string]string{"where": "far", "who": "you"},
		},
		{
			[]string{"where=very=far"},
			map[string]string{"where": "very=far"},
		},
		{
			[]string{"test_params.env"},
			map[string]string{
				"SOME_VAR": "someval",
				"FOO":      "BAR",
				"BAR":      "BAZ",
				"foo":      "bar",
				"bar":      "baz",
			},
		},
		{
			[]string{"test_params.env", "where=far", "who=you"},
			map[string]string{
				"SOME_VAR": "someval",
				"FOO":      "BAR",
				"BAR":      "BAZ",
				"foo":      "bar",
				"bar":      "baz",
				"where":    "far",
				"who":      "you",
			},
		},
	}

	for _, test := range tests {
		out, err := parseParams(test.Input)
		if err != nil {
			t.Errorf("unable to parse params: %s", err)

			break
		}

		if !reflect.DeepEqual(out, test.Output) {
			t.Errorf("wanted params %+v, got %+v", test.Output, out)
		}
	}
}

func Test_getServerWithDefaults(t *testing.T) {
	tests := []struct {
		Server string
		Link   string
		Result string
	}{
		{"", "http://ci.woodpeckerci.org", "http://ci.woodpeckerci.org"},
		{"", "http://ci.woodpeckerci.org:8000", "http://ci.woodpeckerci.org:8000"},
		{"", "https://ci.woodpeckerci.org", "https://ci.woodpeckerci.org"},
		{"", "https://ci.woodpeckerci.org:8888", "https://ci.woodpeckerci.org:8888"},
		{"https://ci.woodpeckerci.org", "https://ci.woodpeckerci.org:8888", "https://ci.woodpeckerci.org"},
	}

	for _, test := range tests {
		server := getServerWithDefaults(test.Server, test.Link)

		if server != test.Result {
			t.Errorf("wanted server url %s, got %s", test.Result, server)
		}
	}
}
