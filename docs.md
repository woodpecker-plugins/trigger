---
name: Trigger
icon: https://codeberg.org/woodpecker-plugins/trigger/media/branch/main/logo.png
description: Plugin to trigger manual or deployment pipelines of a repository.
author: Woodpecker Authors
tags: [woodpecker, trigger, pipeline]
containerImage: woodpeckerci/plugin-trigger
containerImageUrl: https://hub.docker.com/r/woodpeckerci/plugin-trigger
url: https://codeberg.org/woodpecker-plugins/trigger
---

Woodpecker CI plugin to trigger a manual or deployment pipeline of another repository. This plugin is a fork of [drone-plugins/drone-downstream](https://github.com/drone-plugins/drone-downstream/).

## Features

- Trigger one or multiple pipelines
- Trigger as manual or deployment pipeline
- Pass variables to pipelines

## Settings

| Settings Name     | Default        | Description                                                                       |
| ----------------- | -------------- | --------------------------------------------------------------------------------- |
| `server`          | current server | Woodpecker CI server URL to call                                                  |
| `token`           | _none_         | Woodpecker CI API token for server                                                |
| `repositories`    | _none_         | Repository names to trigger pipelines for                                         |
| `deploy`          | _none_         | The environment to deploy to (if not set / empty, triggers a manual pipeline)     |
| `params`          | _none_         | List of params (key=value or file paths of params) to pass to triggered pipelines |
| `params-from-env` | _none_         | List of environment variables to pass to triggered pipelines                      |
| `wait`            | `false`        | To wait for any currently running pipeline to finish                              |
| `timeout`         | `60s`          | How long to wait on any currently running pipelines                               |
| `last-successful` | `false`        | To trigger last successful pipeline                                               |

### repositories

Multiple repositories whose pipelines need to be triggered can be provided.
Also it's possible to provide specific branch to trigger can be specified with `@` separator, ex. `owner/repo@branch`.

## Examples

```yaml
steps:
  name: trigger-downstream:
  image: woodpeckerci/plugin-trigger
  settings:
    repositories:
      - octocat/hello-world@master
      - octocat/sample
    token:
      from_secret: woodpecker_token
```

```yaml
steps:
  - name: publish
    image: woodpeckerci/plugin-trigger
    settings:
      repositories:
        - octocat/hello-world
      deploy: production
      params:
        - DATABASE=prod
      wait: true
      token:
        from_secret: woodpecker_token
    when:
      branch: ${CI_REPO_DEFAULT_BRANCH}
      event: push
```
